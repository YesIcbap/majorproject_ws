# import the streamlit library
import streamlit as st
# for putting the generated music in the fifo queue
from queue import Queue
 
# one for music generation and store the generated music another for playing the music
import _thread


import os
from argparse import ArgumentParser
import numpy as np
import torch

from c_rnn_gan import Generator
import music_data_utils
import pygame
import time


CKPT_DIR = 'models'
G_FN = 'c_rnn_gan_g.pth'

MAX_SEQ_LEN = 256

# initialize the pygame 
pygame.init()

# Initializing a queue
q = Queue(maxsize = 100)

# load and prepare model
dataloader = music_data_utils.MusicDataLoader(datadir=None)
num_feats = dataloader.get_num_song_features()

use_gpu = torch.cuda.is_available()
g_model = Generator(num_feats, use_cuda=use_gpu)

if not use_gpu:
    ckpt = torch.load(os.path.join(CKPT_DIR, G_FN), map_location='cpu')
else:
    ckpt = torch.load(os.path.join(CKPT_DIR, G_FN))

g_model.load_state_dict(ckpt)



# define some function 
def get_new_file_name() :
	if not q.full():
		tempfn = str(time.time())
		fileDirName = "media/gen_music" + tempfn + ".midi"
		# Adding of element to queue
		q.put(fileDirName)
		return fileDirName
	else:
		print("queue is full !!!")
		time.sleep(15)


def return_true_if_music_playing_is_finished():
	if pygame.mixer.music.get_busy():
		print("music is playing")
		return False
	else:
		print("music is finished")
		return True





def remove_music_file(filename):
	os.remove(filename)
	print("file named : {} get removed!!!".format(filename))
	return True


def get_music_file_from_the_queue():
	print("the music file is taken out from the queue")
	return q.get()


def play_the_music_file(filenamedir):
	# play each tone using pygame
	pygame.mixer.music.load(filenamedir)
	pygame.mixer.music.play()
	return True


def generate_music_music_using_the_trained_model(level):
	st.text("music playing")
	g_states = g_model.init_hidden(1)
	z = torch.empty([1, MAX_SEQ_LEN, num_feats]).uniform_() # random vector
	if use_gpu:
	    z = z.cuda()
	    g_model.cuda()
	g_model.eval()
	for i in range(0, int(level) ):
		g_feats, g_states = g_model(z, g_states)
		song_data = g_feats.squeeze().cpu()
		song_data = song_data.detach().numpy() 
		l_filename = get_new_file_name()
		dataloader.save_data(l_filename, song_data)

	return True







# Define a function for the thread 1
def handle_generation_of_music( threadName, level ):
	print("called handle_generation_of_music")
	# write logic to play music
	music_generation_status = generate_music_music_using_the_trained_model(level = level)
	if music_generation_status:
		print("All the music is generated based on the level !!!")
	


# Define a function for the thread 2
def handle_playing_of_music( threadName):
	print("called handle_playing_of_music")
	while True:
		if q.empty():
			print("nothing in the queue!!!")
		
		else:
			if return_true_if_music_playing_is_finished():
				music_file_name_from_queue = get_music_file_from_the_queue()
				play_the_music_file(music_file_name_from_queue)
				print("music is playing from thread : {}".format(threadName))
				remove_music_file(music_file_name_from_queue)










 







# give a title to our app
st.title('BALA music controller')

# slider
  
# first argument takes the title of the slider
# second argument takes thr starting of the slider
# last argument takes the end number
level = st.slider("Select the duration", 1, 50)
  
# print the level
# format() is used to print value 
# of a variable at a specific position
st.text('Selected: {}'.format(level))





# Selection box

# first argument takes the titleof the selectionbox
# second argument takes options
hobby = st.selectbox("Select the music of different flavour: ",
					['classical', 'hiphop', 'jazz'])

# print the selected hobby
st.write("Your hobby is: ", hobby)



# Create a button, that when clicked, shows a text
if(st.button("Play music")):
	# Create two threads as follows
	try:
		_thread.start_new_thread( handle_generation_of_music , ("Thread-1", level, ) )
		_thread.start_new_thread( handle_playing_of_music, ("Thread-2",) )
	except:
		print ("Error: unable to start thread")



	

if(st.button("Stop music")):
	st.text("music stoped")
	# write logic to play music
	if pygame.mixer.get_busy():
		pygame.mixer.music.stop()
	


if(st.button("Pause music")):
	st.text("music get paused")
	# write logic to play music
	if pygame.mixer.get_busy():
		pygame.mixer.pause()


if(st.button("Resume music")):
	st.text("music get resumed")	
	# write logic to play music
	if not pygame.mixer.get_busy():
		pygame.mixer.unpause()



	
if(st.button("Volume up music")):
	st.text("music volume up")
	# write logic to play music
	cv = pygame.mixer.music.get_volume()
	print("current valume = ", cv)
	if cv < 1:
		cv = cv + 0.5
	pygame.mixer.music.set_volume(cv)
	print('Updated volume = ',cv)


        

if(st.button("Volume down music")):
	st.text("music volume down")
	# write logic to play music
	cv = pygame.mixer.music.get_volume()
	print("current valume = ", cv)

	if cv > 0:
		cv = cv - 0.5
	pygame.mixer.music.set_volume(cv)
	print('Updated volume = ',cv)



	
	
	
	
	
	
	
	
	
